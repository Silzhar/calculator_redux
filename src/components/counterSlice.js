import { createSlice } from '@reduxjs/toolkit';

export const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    value: 1, // Valor inicial del contador.
    acc: '1',  // Valor para cambiar con las acciones.
  },
  reducers: {
    add: (state, action) => {
      state.value += Number(state.acc) || 0;
    },
    substract: (state, action) => {
      state.value -= Number(state.acc) || 0;
    },
    multiplication: (state, action) => {
      state.value *= Number(state.acc) || 0;
    },
    division: (state, action) => {
      state.value /= Number(state.acc) || 0;
    },
    changeAcc: (state, action) => {
      state.acc = action.payload;
    }
}});

// Actions.
export const {add, substract, changeAcc, multiplication, division} = counterSlice.actions;


// Selectors. Trae un valor del estado en forma de variable.
// state : estado de Redux.
export const selectValue = (state) => {
  return state.counter.value;
}

export const selectAcc = (state)  => {
  return state.counter.acc;
}

export default counterSlice.reducer;
