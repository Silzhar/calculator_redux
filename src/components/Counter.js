import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { selectValue, add, substract, multiplication, division,
   selectAcc, changeAcc } from '../components/counterSlice'

import './Counter.css'

export function Counter() {
  const value = useSelector(selectValue);
  const dispatch = useDispatch();
  const acc = useSelector(selectAcc);

  return (
    <div>
      <h3>Valor del contador: {value}</h3>

      <div className="Buttons">
        <button className="Buttons_action"
        onClick={() => {
          dispatch(add());
        }}><p>+</p>
          Añadir</button>

        <button className="Buttons_action"
        onClick={() => {
          dispatch(substract());
        }}><p>-</p>
          Eliminar</button>

        <button className="Buttons_action"
        onClick={() => {
          dispatch(multiplication());
        }}><p>x</p>
        Multiplicar</button>

        <button className="Buttons_action"
        onClick={() => {
          dispatch(division());
        }}><p>%</p>
        Dividir</button>
      </div>
  
      <div>
        <h3>Valor a cambiar.</h3>
        <input type="number" value={acc} onChange={(event) => {
          dispatch(changeAcc(event.target.value));
        }}/>
      </div>
    </div>
  )
}
