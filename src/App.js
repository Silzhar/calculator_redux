import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Counter } from './components/Counter';

function App() {
  return (
    <div className="App">
      <header>
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <div>
        <Counter />
      </div>
    </div>
  );
}

export default App;
